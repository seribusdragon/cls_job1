<?php


namespace Drupal\cls_job1\Plugin\Field\FieldFormatter;


use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'cls_str_rot13' formatter.
 *
 * @FieldFormatter(
 *   id = "cls_str_rot13",
 *   label = @Translation("ROT13  Converstion of text block"),
 *   field_types = {
 *     "text"
 *   }
 * )
 */
class cls_str_rot13 extends FormatterBase {

  private array $search1 = [];

  private array $search2 = [];

  private array $searchhold1 = [];

  private array $searchhold2 = [];

  private array $replace1 = [];

  private array $replace2 = [];

  function __construct($plugin_id, $plugin_definition,
                       FieldDefinitionInterface $field_definition,
                       array $settings, $label, $view_mode,
                       array $third_party_settings) {
    /*
    The arrays here could also be hard coded to add some efficiencies.
    This also only works with letters and could be extended to include
    numbers and special char.
    */
    parent::__construct($plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings);
    if (empty($this->search1) || empty($this->search2) ||
      empty($this->replace1) || empty($this->replace2)) {
      $elementSearchSet1 = [];
      $elementSearchSet2 = [];
      $elementReplaceSet1 = [];
      $elementReplaceSet2 = [];
      foreach (range('A', 'Z') as $element) {
        $elementSearchSet1[] = $element;
        $elementSearchSet1hold[] = $element . '1';
      }
      foreach (range('a', 'z') as $element) {
        $elementSearchSet2[] = $element;
        $elementSearchSet2hold[] = $element . '1';
      }
      $countReplace = count($elementSearchSet1);
      foreach ($elementSearchSet1 as $index => $item) {
        $replaceIndex = $index + 13;

        if ($replaceIndex >= $countReplace) {
          $replaceIndex = $replaceIndex - $countReplace;
        }

        $elementReplaceSet1[$index] = $elementSearchSet1[$replaceIndex];
      }
      $countReplace = count($elementSearchSet2);
      foreach ($elementSearchSet2 as $index => $item) {
        $replaceIndex = $index + 13;
        if ($replaceIndex >= $countReplace) {
          $replaceIndex = $replaceIndex - $countReplace;
        }

        $elementReplaceSet2[$index] = $elementSearchSet2[$replaceIndex];
      }
      $this->search1 = $elementSearchSet1;
      $this->replace1 = $elementReplaceSet1;
      $this->search2 = $elementSearchSet2;
      $this->replace2 = $elementReplaceSet2;
      /*
       *  The hold array to to remove the problem of the search and replace
       * going back on itself and replacing things that have already been replaced.
      */
      /* TODO:Come back and find better solution */
      $this->searchhold1 = $elementSearchSet1hold;
      $this->searchhold2 = $elementSearchSet2hold;
    }

  }

  public function replaceStrROT13(string $value): string {
    //Todo:  You could Use regex here but I am not sure if it would be better
    $value = str_replace($this->search1, $this->searchhold1, $value);
    $value = str_replace($this->searchhold1, $this->replace1, $value);
    $value = str_replace($this->search2, $this->searchhold2, $value);
    $value = str_replace($this->searchhold2, $this->replace2, $value);
    return $value;
  }

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = ['#markup' => $this->replaceStrROT13($item->value)];
    }

    return $element;
  }

}