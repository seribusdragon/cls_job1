<?php


namespace Drupal\cls_job1\Plugin\Field\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Cocur\Slugify\Slugify;
/**
* Plugin implementation of the 'cls_slugify' formatter.
 *
 * @FieldFormatter(
 *   id = "cls_slugify",
 *   label = @Translation("Sligify text"),
 *   field_types = {
  *     "text"
  *   }
 * )
 */

class cls_slugify extends FormatterBase{

  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var  \Cocur\Slugify\Slugify $slug */
    $slug = \Drupal::getContainer()
      ->get('slugify.process');
    $test = $slug->slugify('Hello World','-');
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = ['#markup' => $test];
    }

    return $element;
    // TODO: Implement viewElements() method.
  }

}